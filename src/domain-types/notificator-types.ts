import {ObjectId, InsertManyResult} from "mongodb";
import {PatientTypes} from ".";

export namespace NotificatorTypes {
    export interface IPatientEmailNotificatorService {
        scheduleEmailsForPatients(patients: PatientTypes.IPatient[]): Promise<void>;
        // possible implementation ...
        // notifyByEmail();
    }

    export interface IEmailRepository {
        insertMany(emails: IEmail[]): Promise<InsertManyResult>;
    }

    export type IEmail = {
        _id: ObjectId;
        name: string;
        subject: string;
        body: string;
        patientId: ObjectId;
        scheduledDate: Date;
    }
}
