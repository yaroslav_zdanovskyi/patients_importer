import {ObjectId} from "mongodb";

export namespace ImporterTypes {
    export interface IPatientCSVImporterService {
        importPatientsFromCSV(CSVFileName: string): Promise<ImporterTypes.PatientImporterOutput>;
    }

    export type PatientImporterOutput = {
        missingFirstName: ObjectId[];
        missingEmailButHasConsent: ObjectId[];
    }

    export const CSVIndexMap = {
        programIdentifier: 0,
        dataSource: 1,
        cardNumber: 2,
        memberId: 3,
        firstName: 4,
        lastName: 5,
        dateOfBirth: 6,
        address1: 7,
        address2: 8,
        city: 9,
        state: 10,
        zipCode: 11,
        telephoneNumber: 12,
        emailAddress: 13,
        consent: 14,
        mobilePhone: 15,
    }
}
