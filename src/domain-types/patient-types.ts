import {ObjectId} from "mongodb";

export namespace PatientTypes {
    export interface IPatientRepository {
        insertMany(patients: IPatient[]);
    }

    export type IPatient = {
        _id: ObjectId;
        programIdentifier: number;
        dataSource: string;
        cardNumber: number;
        memberId: number;
        firstName?: string;
        lastName?: string;
        dateOfBirth: Date;
        address1: string;
        address2?: string;
        city: string;
        state: string;
        zipCode: number;
        telephoneNumber?: string;
        emailAddress?: string;
        consent: string;
        mobilePhone: string;
    }

    export enum ConsentType {
        YES = 'Y',
        NO = 'N'
    }
}
