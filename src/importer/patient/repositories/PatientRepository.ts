import {PatientTypes} from "../../../domain-types";
import {Collection} from "mongodb";

export class PatientRepository implements PatientTypes.IPatientRepository {
    constructor(private readonly collection: Collection) {}

    async insertMany(patients: PatientTypes.IPatient[]) {
        await this.collection.insertMany(patients);
    }
}
