import {ImporterTypes as IT, PatientTypes, NotificatorTypes} from "../../../domain-types";
import {parse as parseSync} from 'csv-parse/sync';
import {readFileSync} from "fs";
import {ObjectId} from "mongodb";
import * as path from "path";
import * as moment from "moment-timezone";

export class ImporterService implements IT.IPatientCSVImporterService {
    constructor(
        private readonly patientRepository: PatientTypes.IPatientRepository,
        private readonly patientEmailNotificator: NotificatorTypes.IPatientEmailNotificatorService
    ) {}

    async importPatientsFromCSV(CSVFileName: string): Promise<IT.PatientImporterOutput> {
        const CSVPath = path.resolve(`./data/csv/${CSVFileName}`);
        const fileContent = readFileSync(CSVPath).toString();

        const patientRows = parseSync(fileContent, {
            delimiter: '|'
        });

        // exclude header info
        patientRows.shift();

        let patientsToNotify: PatientTypes.IPatient[] = [];
        let missingFirstName: ObjectId[] = [];
        let missingEmailButHasConsent: ObjectId[] = [];

        for (let patientRow of patientRows) {
            const patient: PatientTypes.IPatient = {
                _id: new ObjectId(),
                programIdentifier: patientRow[IT.CSVIndexMap.programIdentifier],
                dataSource: patientRow[IT.CSVIndexMap.dataSource],
                cardNumber: patientRow[IT.CSVIndexMap.cardNumber],
                memberId: patientRow[IT.CSVIndexMap.memberId],
                firstName: patientRow[IT.CSVIndexMap.firstName] ?? null,
                lastName: patientRow[IT.CSVIndexMap.lastName] ?? null,
                dateOfBirth: moment.tz(patientRow[IT.CSVIndexMap.dateOfBirth], 'MM/DD/YYYY', 'UTC').toDate(),
                address1: patientRow[IT.CSVIndexMap.address1],
                address2: patientRow[IT.CSVIndexMap.address2] ?? null,
                city: patientRow[IT.CSVIndexMap.city],
                state: patientRow[IT.CSVIndexMap.state],
                zipCode: patientRow[IT.CSVIndexMap.zipCode],
                telephoneNumber: patientRow[IT.CSVIndexMap.telephoneNumber] ?? null,
                emailAddress: patientRow[IT.CSVIndexMap.emailAddress] ?? null,
                consent: patientRow[IT.CSVIndexMap.consent],
                mobilePhone: patientRow[IT.CSVIndexMap.mobilePhone],
            };

            if (!patient.firstName.length) {
                missingFirstName.push(patient._id);
            }

            if (patient.consent === PatientTypes.ConsentType.YES) {
                patientsToNotify.push(patient);

                if (!patient.emailAddress.length) {
                    missingEmailButHasConsent.push(patient._id);
                }
            }

            await this.patientRepository.insertMany([patient]);
        }

        if (patientsToNotify.length) {
            await this.patientEmailNotificator.scheduleEmailsForPatients(patientsToNotify);
        }

        return {
            missingFirstName,
            missingEmailButHasConsent
        }
    }
}
