import {NotificatorTypes, PatientTypes} from "../../../domain-types";
import {ObjectId} from "mongodb";
import * as moment from "moment-timezone";

export class NotificatorService implements NotificatorTypes.IPatientEmailNotificatorService{
    constructor(
        private readonly patientRepository: NotificatorTypes.IEmailRepository,
    ) {}

    async scheduleEmailsForPatients(patients: PatientTypes.IPatient[]): Promise<void> {
        for (const patient of patients) {
            const date = moment();

            const emailsForPatient = [
                {
                    _id: new ObjectId(),
                    name: 'Day 1',
                    subject: 'Day 1',
                    body: 'Day 1',
                    patientId: patient._id,
                    scheduledDate: date.add(1, 'days').toDate()
                },
                {
                    _id: new ObjectId(),
                    name: 'Day 2',
                    subject: 'Day 2',
                    body: 'Day 2',
                    patientId: patient._id,
                    scheduledDate: date.add(1, 'days').toDate()
                },
                {
                    _id: new ObjectId(),
                    name: 'Day 3',
                    subject: 'Day 3',
                    body: 'Day 3',
                    patientId: patient._id,
                    scheduledDate: date.add(1, 'days').toDate()
                },
                {
                    _id: new ObjectId(),
                    name: 'Day 4',
                    subject: 'Day 4',
                    body: 'Day 4',
                    patientId: patient._id,
                    scheduledDate: date.add(1, 'days').toDate()
                }
            ];

            await this.patientRepository.insertMany(emailsForPatient);
        }

        return;
    }
}
