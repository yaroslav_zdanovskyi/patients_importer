import {NotificatorTypes} from "../../../../domain-types";
import {Collection, InsertManyResult} from "mongodb";

export class EmailRepository implements NotificatorTypes.IEmailRepository {
    constructor(private readonly collection: Collection) {}

    async insertMany(emails: NotificatorTypes.IEmail[]): Promise<InsertManyResult> {
        return await this.collection.insertMany(emails);
    }
}
