import {Providers} from "./providers";
import {ImporterTypes, NotificatorTypes} from "../domain-types";

export class ServiceLocator {
    async PatientCSVImporter(): Promise<ImporterTypes.IPatientCSVImporterService> {
        let provider = await this.getProvider("PatientCSVImporter");
        return provider(this);
    }

    async PatientEmailNotificator(): Promise<NotificatorTypes.IPatientEmailNotificatorService> {
        let provider = await this.getProvider("PatientEmailNotificator");
        return provider(this);
    }

    async MongoDB() {
        let provider = await this.getProvider("MongoDB");
        return provider(this);
    }

    private async getProvider(providerName: string) {
        // can also be implemented as a singleton
        return Providers[providerName];
    }
}
