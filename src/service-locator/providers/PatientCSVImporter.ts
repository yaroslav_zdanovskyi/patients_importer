import {ServiceLocator} from "../";
import {ImporterTypes} from "../../domain-types";
import {ImporterService} from "../../importer/patient/csv";
import {PatientRepository} from "../../importer/patient/repositories/PatientRepository";

export default async (SL: ServiceLocator): Promise<ImporterTypes.IPatientCSVImporterService> => {
    const mongoDB = await SL.MongoDB();

    const patientEmailNotificator = await SL.PatientEmailNotificator();

    const patientCollection = await mongoDB.collection(process.env.PATIENTS_COLLECTION_NAME)

    const patientRepository = new PatientRepository(patientCollection);

    return new ImporterService(patientRepository, patientEmailNotificator);
}
