import {ServiceLocator} from "../";
import {NotificatorTypes} from "../../domain-types";
import {NotificatorService} from "../../notificator/patient/email";
import {EmailRepository} from "../../notificator/patient/email"

export default async (SL: ServiceLocator): Promise<NotificatorTypes.IPatientEmailNotificatorService> => {
    const mongoDB = await SL.MongoDB();

    const emailsCollection = await mongoDB.collection(process.env.EMAILS_COLLECTION_NAME)

    const patientRepository = new EmailRepository(emailsCollection);

    return new NotificatorService(patientRepository);
}
