import {ServiceLocator} from "../";
import {Db, MongoClient} from "mongodb";

export default async (SL: ServiceLocator): Promise<Db> => {
    const mongoClient = new MongoClient(process.env.PATIENTS_LOCAL_DB_CONNECTION_STRING);
    await mongoClient.connect();

    return mongoClient.db();
}
