import PatientCSVImporter from "./PatientCSVImporter";
import PatientEmailNotificator from "./PatientEmailNotificator";
import MongoDB from "./MongoDB";

export const Providers: any = {
    PatientCSVImporter,
    PatientEmailNotificator,
    MongoDB
}
