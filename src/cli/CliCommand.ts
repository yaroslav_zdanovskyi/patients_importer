import {ServiceLocator} from "../service-locator";

export abstract class CliCommand {
    constructor(protected SL: ServiceLocator) {}

    abstract run(): Promise<void>;
}
