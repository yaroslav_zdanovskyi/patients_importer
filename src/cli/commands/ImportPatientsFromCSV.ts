import {CliCommand} from "../CliCommand";

export class ImportPatientsFromCSV extends CliCommand {
    async run(): Promise<void> {
        const patientCSVImporter = await this.SL.PatientCSVImporter();

        const CSVFileName = 'patients.csv';

        const importerOutput = await patientCSVImporter.importPatientsFromCSV(CSVFileName);

        console.log(importerOutput);
    }
}
