import {ServiceLocator} from "../service-locator";
import * as path from "path";
require("dotenv").config();

(async () => {
    const commandName = process.argv[2];

    const SL = new ServiceLocator();

    const commands = require(path.resolve(__dirname + `/commands/${commandName}`));
    const commandObject = new commands[commandName](SL);

    commandObject.run();
})();
